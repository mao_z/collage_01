worker_processes 3
preload_app true
timeout 1200
if ENV['RAILS_ENV']=='development'

else
  listen '/home/ruby/villex.ru/collage_01/sockets/collage_01.sock' # listen on the given Unix domain socket
  stderr_path '/home/ruby/villex.ru/collage_01/master.unicorn.log'
  stdout_path '/home/ruby/villex.ru/collage_01/master.unicorn.log'
  pid '/home/ruby/villex.ru/collage_01/unicorn.dpid'
end
