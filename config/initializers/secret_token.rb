# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Library::Application.config.secret_key_base = 'bc74b043e3dfff8d7db2377f3912e49e8137aef1fb79fa19d5ae6a9dc73243a513890082b7188f324fd22b59158a7dd39de9f0f547c03aa58825bfd471716bc4'
