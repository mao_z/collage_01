#!/bin/env ruby
# encoding: utf-8
class UserBook < ActiveRecord::Base
  belongs_to :user
  belongs_to :book

  after_create :update_user_book_count

  def update_user_book_count
    self.user.books_count = UserBook.where(:user => self.user).count
    self.book.status |= Book::STATUS_GIVEN
    self.book.save!
    self.user.save!
  end
end
