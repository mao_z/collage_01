#!/bin/env ruby
# encoding: utf-8
class User < ActiveRecord::Base
  validates_presence_of :name, :fname, :lname, :dob, :address
  validate :phone_work, :presence if 'phone_home.nil?'
  validate :phone_home, :presence if 'phone_work.nil?'
  validate :check_age
  validates_numericality_of :phone_work, :phone_home, {:only_integer => true, :allow_blank => true}
  validates :name, :uniqueness => {
      :scope => :lname,
      :message => "#{name} already registred"
  }

  has_many :user_books
  has_many :books, :through => :user_books


  def check_age
    errors.add(:discount, "Must be at least 17 ears old") if dob>17.years.ago
  end

  def valid_name
    "#{name} #{fname} #{lname}"
  end
end
