#!/bin/env ruby
# encoding: utf-8

class BookGenre < ActiveRecord::Base
  belongs_to :genre
  belongs_to :book_info
end
