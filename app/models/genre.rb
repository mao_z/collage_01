#!/bin/env ruby
# encoding: utf-8
class Genre < ActiveRecord::Base
  has_many :book_genres
  has_many :book_infos, :through => :book_genres
end
