#!/bin/env ruby
# encoding: utf-8
class Book < ActiveRecord::Base
  belongs_to :book_info

  validates_presence_of :isbn, :book_info_id, :place

  STATUS_GIVEN = 1

  validates_presence_of :isbn, :place, :book_info_id

  def book_ident
    "#{book_info.title} (isbn: #{isbn})"
  end
end
