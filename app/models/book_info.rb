#!/bin/env ruby
# encoding: utf-8
class BookInfo < ActiveRecord::Base

  has_many :books
  has_many :book_genres
  has_many :genres, :through => :book_genres

  validates_presence_of :title, :publish_city, :publish_year, :publisher, :pages, :cost, :genres

  AUTHORS = ['Пушкин','Толстой','Стариков','Ролинг','Лермонтов','Гоголь']

end
