json.array!(@book_infos) do |book_info|
  json.extract! book_info, :id, :title, :author, :publish_city, :publisher, :publish_year, :pages, :cost
  json.url book_info_url(book_info, format: :json)
end
