json.array!(@books) do |book|
  json.extract! book, :id, :book_info_id, :hash, :status, :place
  json.url book_url(book, format: :json)
end
