json.array!(@users) do |user|
  json.extract! user, :id, :name, :fname, :lname, :address, :phone_home, :phone_work, :dob, :status
  json.url user_url(user, format: :json)
end
