json.array!(@book_genres) do |book_genre|
  json.extract! book_genre, :id, :genre_id, :book_info_id
  json.url book_genre_url(book_genre, format: :json)
end
