# encoding: utf-8

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Genre.all.delete_all
Book.all.delete_all
User.all.delete_all
BookInfo.all.delete_all

Genre.create([{name:"Фантастика"},{name:"Классика"},{name:"Политика"},{name:"Экономика"},{name:"Сказка"}])

BookInfo.create([
                    {
                        :title => 'Война и Мир',
                        :author => 'Толстой',
                        :publish_city => 'Ленинград',
                        :publisher => 'Питер',
                        :publish_year => 1965,
                        :genres => [Genre.find_by_name('Классика')],
                        :pages => 12312,
                        :cost => 12.6
                    },
                    {
                        :title => 'Власть',
                        :author => 'Стариков',
                        :publish_city => 'Ленинград',
                        :publisher => 'Питер',
                        :publish_year => 2015,
                        :genres => [Genre.find_by_name('Политика'), Genre.find_by_name('Экономика')],
                        :pages => 543,
                        :cost => 150
                    },
                    {
                        :title => 'Сказка о золотой рыбке',
                        :author => 'Пушкин',
                        :publish_city => 'Ленинград',
                        :publisher => 'Питер',
                        :publish_year => 1965,
                        :genres => [Genre.find_by_name('Сказка')],
                        :pages => 12,
                        :cost => 5
                    },
                    {
                        :title => 'Гарри Поттер',
                        :author => 'Ролинг',
                        :publish_city => 'Лондон',
                        :publisher => 'AssBook',
                        :publish_year => 1965,
                        :genres => [Genre.find_by_name('Фантастика'), Genre.find_by_name('Сказка')],
                        :pages => 456,
                        :cost => 12.6
                    },
                    {
                        :title => 'Герой нашего времени',
                        :author => 'Лермонтов',
                        :publish_city => 'Ленинград',
                        :publisher => 'Питер',
                        :publish_year => 1965,
                        :genres => [Genre.find_by_name('Классика')],
                        :pages => 2312,
                        :cost => 12.6
                    },
                ])
Book.create([
                {
                    :book_info_id => BookInfo.find_by_title('Война и Мир').id,
                    :isbn => '2-266-11156-6',
                    :place => 'А7-34'
                },
                {
                    :book_info_id => BookInfo.find_by_title('Война и Мир').id,
                    :isbn => '2-266-11156-6',
                    :place => 'О9-12'
                },
                {
                    :book_info_id => BookInfo.find_by_title('Власть').id,
                    :isbn => '2-266-11156-6',
                    :place => 'Ф7-31'
                },
                {
                    :book_info_id => BookInfo.find_by_title('Сказка о золотой рыбке').id,
                    :isbn => '2-266-11156-6',
                    :place => 'С3-23'
                },
                {
                    :book_info_id => BookInfo.find_by_title('Гарри Поттер').id,
                    :isbn => '2-266-11156-6',
                    :place => 'У5-34'
                },
                {
                    :book_info_id => BookInfo.find_by_title('Гарри Поттер').id,
                    :isbn => '2-266-11156-6',
                    :place => 'О3-3'
                },
                {
                    :book_info_id => BookInfo.find_by_title('Герой нашего времени').id,
                    :isbn => '2-266-11156-6',
                    :place => 'А6-45'
                },
            ])
User.create([
                {
                    :name => 'Агадия',
                    :fname => 'Акакиевна',
                    :lname => 'Махбубанова',
                    :address => 'Поляна около Ашана',
                    :phone_work => '79006543465',
                    :dob => 19.years.ago
                },
                {
                    :name => 'Кунигунда',
                    :fname => 'Маусукович',
                    :lname => 'Махбубанов',
                    :address => 'Автовокзал',
                    :phone_work => '72347658877',
                    :dob => 23.years.ago
                },
                {
                    :name => 'Махбубан',
                    :fname => 'Кунигундович',
                    :lname => 'Попиков',
                    :address => 'Рязань-2',
                    :phone_work => '72345553344',
                    :dob => 97.years.ago
                },
                {
                    :name => 'Пенелопа',
                    :fname => 'Васильевна',
                    :lname => 'Синь',
                    :address => 'Рязань-1, у входа',
                    :phone_work => '72345563345',
                    :dob => 21.years.ago
                },
                {
                    :name => 'Як',
                    :fname => 'Ку',
                    :lname => 'Дза',
                    :address => 'Вход в М5 молл',
                    :phone_work => '75463452808',
                    :dob => 42.years.ago
                },
            ])