class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :fname
      t.string :lname
      t.string :address
      t.string :phone_home
      t.string :phone_work
      t.date :dob
      t.integer :status, :default => 0

      t.timestamps
    end
  end
end
