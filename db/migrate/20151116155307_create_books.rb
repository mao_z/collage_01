class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.references :book_info, application: true
      t.string :hash
      t.integer :status, :default => 0
      t.string :place

      t.timestamps
    end
  end
end
