class CreateBookGenres < ActiveRecord::Migration
  def change
    remove_column :book_infos, :genre_id
    add_column :book_infos, :book_genre_id, :integer
    create_table :book_genres do |t|
      t.references :genre, index: true
      t.references :book_info, index: true

      t.timestamps
    end
  end
end
