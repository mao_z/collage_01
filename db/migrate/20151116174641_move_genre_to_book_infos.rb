class MoveGenreToBookInfos < ActiveRecord::Migration
  def change
    remove_column :books, :genre
    add_reference :book_infos, :genre
  end
end
