class CreateBookInfos < ActiveRecord::Migration
  def change
    create_table :book_infos do |t|
      t.string :title
      t.string :author
      t.string :publish_city
      t.string :publisher
      t.integer :publish_year
      t.integer :pages
      t.float :cost

      t.timestamps
    end
  end
end
