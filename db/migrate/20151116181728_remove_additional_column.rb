class RemoveAdditionalColumn < ActiveRecord::Migration
  def change
    remove_column :book_infos, :book_genre_id
  end
end
