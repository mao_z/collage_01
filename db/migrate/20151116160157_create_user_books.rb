class CreateUserBooks < ActiveRecord::Migration
  def change
    create_table :user_books do |t|
      t.references :user, application: true
      t.references :book, application: true
      t.datetime :expires_at
      t.integer :status, :default => 0

      t.timestamps
    end
  end
end
