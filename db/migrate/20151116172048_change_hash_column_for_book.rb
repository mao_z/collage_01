class ChangeHashColumnForBook < ActiveRecord::Migration
  def change
    remove_column :books, :hash
    add_column :books, :isbn, :string
  end
end
